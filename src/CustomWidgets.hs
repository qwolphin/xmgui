{-# LANGUAGE OverloadedStrings, OverloadedLabels #-}
module CustomWidgets
    (colorSelector,
     integerSelector
    ) where
import qualified GI.Gtk as Gtk
import qualified GI.Gdk as Gdk

import Data.GI.Base
import Data.Text.Internal (Text)
import Data.Text (unpack)

settingsEntry :: Gtk.IsWidget w => Text -> w -> IO Gtk.Box
settingsEntry name subWidget = do
  container <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal,
                             #spacing := 10 ]
  set container [ #marginStart := 10,
                  #marginTop := 10,
                  #marginBottom := 10,
                  #marginEnd := 10]
  label <- new Gtk.Label [ #label := name]

  #packStart container label False False 10
  #packEnd container subWidget False False 10
  return container

colorSelector :: Text -> Text -> IO (Gtk.Box, Gtk.ColorButton)
colorSelector name colorString = do
  color <- new Gdk.RGBA []
  colorParseSuc <- Gdk.rGBAParse color colorString
  selector <- new Gtk.ColorButton [ #rgba := color]
  container <- settingsEntry name selector
  return (container, selector)

integerSelector :: Text -> Double -> Double -> Double -> IO (Gtk.Box, Gtk.SpinButton)
integerSelector name min max value = do
  aj <- new Gtk.Adjustment [ #lower := min,
                             #upper := max,
                             #stepIncrement := 1.0,
                             #value := value,
                             #pageIncrement := 1.0,
                             #pageSize := 0.0]

  sButton <- new Gtk.SpinButton [ #adjustment := aj,
                                  #climbRate := 1.0,
                                  #digits := 0]

  container <- settingsEntry name sButton
  return (container, sButton)

integerSelector :: Text  -> Double -> IO (Gtk.Box, Gtk.SpinButton)
integerSelector name min max value = do
  aj <- new Gtk.Adjustment [ #lower := min,
                             #upper := max,
                             #stepIncrement := 1.0,
                             #value := value,
                             #pageIncrement := 1.0,
                             #pageSize := 0.0]

  sButton <- new Gtk.SpinButton [ #adjustment := aj,
                                  #climbRate := 1.0,
                                  #digits := 0]

  container <- settingsEntry name sButton
  return (container, sButton)
