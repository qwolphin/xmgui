module ManageHooks (
    ManageHook
  , HookSelector
  , HookAction
  , parseHooks
  , generateHooks
) where
import Language.Haskell.Syntax
import Control.Monad

data ManageHook = ManageHook HookSelector HookAction
  deriving (Eq, Show)

data HookSelector = ClassName String |
                    Resource String  |
                    Title String     |
                    IsFullScreen
                    deriving (Eq, Show)

data HookAction = Shift String |
                  Ignore       |
                  Float        |
                  FullFloat
                  deriving (Eq, Show)

parseHooks :: HsExp -> Maybe [ManageHook]
parseHooks (HsList a) = (sequence . (map parseHook)) a

parseHook :: HsExp -> Maybe ManageHook
parseHook (HsInfixApp rawSelector (HsQVarOp (UnQual (HsSymbol "-->"))) rawAction) =
  liftM2 ManageHook (parseHookSelector rawSelector) (parseHookAction rawAction)

parseHookSelector :: HsExp -> Maybe HookSelector
parseHookSelector (HsInfixApp (HsVar (UnQual (HsIdent "className")))
                              (HsQVarOp (UnQual (HsSymbol "=?")))
                              (HsLit (HsString name))) = Just $ ClassName name
parseHookSelector (HsInfixApp (HsVar (UnQual (HsIdent "title")))
                              (HsQVarOp (UnQual (HsSymbol "=?")))
                              (HsLit (HsString name))) = Just $ Title name
parseHookSelector (HsInfixApp (HsVar (UnQual (HsIdent "resource")))
                              (HsQVarOp (UnQual (HsSymbol "=?")))
                              (HsLit (HsString name))) = Just $ Resource name
parseHookSelector (HsCon (UnQual (HsIdent "IsFullScreen"))) = Just $ IsFullScreen
parseHookSelector _ = Nothing

parseHookAction :: HsExp -> Maybe HookAction
parseHookAction (HsApp (HsVar (UnQual (HsIdent "doShift")))
                       (HsLit (HsString workspaceName))) = Just $ Shift workspaceName
parseHookAction (HsVar (UnQual (HsIdent "doIgnore"))) = Just $ Ignore
parseHookAction (HsVar (UnQual (HsIdent "doFloat"))) = Just $ Float
parseHookAction (HsVar (UnQual (HsIdent "doFullFloat"))) = Just $ FullFloat
parseHookAction _ = Nothing

generateHooks :: [ManageHook] -> HsExp
generateHooks hookList = HsList (map generateHook hookList)

generateHook :: ManageHook -> HsExp
generateHook (ManageHook selector action) =
  (HsInfixApp  (generateHookSelector selector)
               (HsQVarOp (UnQual (HsSymbol "-->")))
               (generateHookAction action))

generateHookSelector :: HookSelector -> HsExp
generateHookSelector (ClassName name) =
  (HsInfixApp (HsVar (UnQual (HsIdent "className")))
                             (HsQVarOp (UnQual (HsSymbol "=?")))
                             (HsLit (HsString name)))
generateHookSelector (Resource name) =
  (HsInfixApp (HsVar (UnQual (HsIdent "resource")))
                              (HsQVarOp (UnQual (HsSymbol "=?")))
                              (HsLit (HsString name)))
generateHookSelector (Title name) =
  (HsInfixApp (HsVar (UnQual (HsIdent "title")))
                              (HsQVarOp (UnQual (HsSymbol "=?")))
                              (HsLit (HsString name)))
generateHookSelector IsFullScreen = HsCon (UnQual (HsIdent "IsFullScreen"))

generateHookAction :: HookAction -> HsExp
generateHookAction (Shift workspaceName) =
  (HsApp (HsVar (UnQual (HsIdent "doShift")))
                        (HsLit (HsString workspaceName)))
generateHookAction Ignore = HsVar (UnQual (HsIdent "doIgnore"))
generateHookAction Float = HsVar (UnQual (HsIdent "doFloat"))
generateHookAction FullFloat = HsVar (UnQual (HsIdent "doFullFloat"))
