module ConfigParser(
    ManageHook
  , HookSelector
  , HookAction
  , parseHooks
  , generateHooks
)  where
import Language.Haskell.Syntax
import ManageHooks
import ConfigData
import Data.Maybe

getDecls :: HsModule -> [HsDecl]
getDecls (HsModule _ _ _ _ decls) = decls

getValueByVarName :: String -> [HsDecl] -> Maybe HsExp
getValueByVarName _ [] = Nothing
getValueByVarName name ((HsPatBind _ (HsPVar (HsIdent varname)) (HsUnGuardedRhs val) _):decls)
  | name == varname = Just val
  | otherwise = getValueByVarName name decls
getValueByVarName name (_:decls) = getValueByVarName name decls



showDecls :: HsModule -> IO ()
showDecls m = do
  let decls = getDecls m
  mapM_ (\a -> print a >> putStrLn "") decls
