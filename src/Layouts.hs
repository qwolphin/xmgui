module Layouts (
    Layout
  , LayoutParameter
) where
import Language.Haskell.Syntax
import Data.Ratio
import Control.Monad
import System.IO.Unsafe

data LayoutPattern = LayoutPattern
                     { pName :: String
                     , pParameters :: [LPPattern] }
                     deriving Show

data LPPattern = RawIntParameter String | RawRatParameter String
  deriving Show

data Layout = Layout { name :: String
                     , parameters :: [LayoutParameter]
                     , mirrored :: Bool
                     , noBorders :: Bool }
                     deriving Show

data LayoutParameter = IntParameter String Integer | RatParameter String Rational
  deriving Show

parseLayouts :: HsExp -> Maybe [Layout]
parseLayouts (HsInfixApp head
                         (HsQVarOp (UnQual (HsSymbol "|||")))
                         tail) = liftM2 (:) (parseLayout head) (parseLayouts tail)
parseLayouts a = mapM parseLayout [a]


parseLayout :: HsExp -> Maybe Layout
parseLayout exp

layoutModifyers :: HsExp -> (Bool, Bool, HsExp)
layoutModifyers (HsApp (HsVar (UnQual (HsIdent "noBorders")))
                       rest)




