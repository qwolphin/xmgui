#
# spec file for package ghc-xmgui
#
# Copyright (c) 2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

%global pkg_name xmgui

Name:           ghc-%{pkg_name}
Version:        0.1.0.0
Release:        0.20170130
Summary:        Xmonad configuration tool with GUI
Group:          System/Libraries

License:        GPL-3+
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkg_name}-%{version}/%{pkg_name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  chrpath
BuildRequires:  ghc-gi-gdk-devel
BuildRequires:  ghc-gi-glib-devel
BuildRequires:  ghc-gi-gtk-devel
BuildRequires:  ghc-haskell-gi-base-devel
BuildRequires:  ghc-text-devel
# End cabal-rpm deps

%description
Please see README.md.


%package devel
Summary:        Haskell %{pkg_name} library development files
Group:          Development/Libraries/Other
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
Requires:       %{name} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%ghc_lib_build


%install
%ghc_lib_install

%ghc_fix_dynamic_rpath %{pkg_name}


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%defattr(-,root,root,-)
%doc LICENSE


%files devel -f %{name}-devel.files
%defattr(-,root,root,-)
%doc README.md
%{_bindir}/%{pkg_name}


%changelog
