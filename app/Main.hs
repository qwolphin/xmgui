{-# LANGUAGE OverloadedStrings, OverloadedLabels #-}
module Main where

import qualified GI.Gtk as Gtk
import qualified GI.Gdk as Gdk
import CustomWidgets
import Data.GI.Base
import Data.IORef


main :: IO ()
main = do
  --create global data
  log <- newIORef ("" :: String)

  Gtk.init Nothing
  --create main window
  win <- new Gtk.Window [ #title := "Xmonad configuration GUI",
                          #defaultWidth := 1024,
                          #defaultHeight := 768 ]
  on win #destroy (Gtk.mainQuit >> readIORef log >>= putStrLn)

  --add stack container
  stack <- new Gtk.Stack []
  sidebar <- new Gtk.StackSidebar [ #stack := stack ]
  container <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]

  #packStart container sidebar False False 0
  #packEnd container stack True True 0
  #add win container

  appearance stack log --appearance tab
  layouts stack log --layouts tab

  --Show window and start mainloop
  #showAll win
  Gtk.main

appearance :: Gtk.Stack -> IORef String -> IO ()
appearance stack log = do
  apnContainer <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #addTitled stack apnContainer "Tile borders" "Tile borders"

  (nBorderColorC, nBorderColorS) <- colorSelector "Normal Color" "#eeeeee"
  #packStart apnContainer nBorderColorC False False 0
  on nBorderColorS #colorSet (amIORef log (++ "color set"))
  (fBorderColorC, fBorderColorS) <- colorSelector "Focused Color" "#df56a3"
  #packStart apnContainer fBorderColorC False False 0

  (borderWidthC, borderWidthS) <- integerSelector "Width" 1.0 100.0 1.0
  #packStart apnContainer borderWidthC False False 0

layouts :: Gtk.Stack -> IORef String -> IO ()
layouts stack log = do
  layContainer <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]
  #addTitled stack layContainer "Layouts" "Layouts"

  layoutsC <- new Gtk.Box [ #orientation := Gtk.OrientationVertical ]
  #packStart layContainer layoutsC False False 0

  layouts <- new Gtk.ListBox []
  #packStart layoutsC layouts True True 0

  acBar <- new Gtk.ActionBar []
  #packEnd layoutsC acBar False False 0
  currentLayout <- new Gtk.Box [ #orientation := Gtk.OrientationHorizontal ]
  #packEnd layContainer currentLayout False False 0

amIORef :: IORef a -> (a -> a) -> IO ()
amIORef ref fn = do
  let helper a = (a, ()) in
      atomicModifyIORef' ref (helper . fn)
